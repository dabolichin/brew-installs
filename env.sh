#!/bin/zsh

# PATH
export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export EDITOR='code'
# export PYTHONPATH=$PYTHONPATH
# export MANPATH="/usr/local/man:$MANPATH"

# Virtual Environment
export PROJECT_HOME=$HOME/Projects

# Owner
export USER_NAME="dabolichin"
#eval "$(rbenv init -)"

# No message editing on merge
export GIT_MERGE_AUTOEDIT=no

# FileSearch
function f() { find . -iname "*$1*" ${@:2} }
function r() { grep "$1" ${@:2} -R . }

#mkdir and cd
function mkcd() { mkdir -p "$@" && cd "$_"; }

# Aliases
alias cppcompile='c++ -std=c++11 -stdlib=libc++'

# Use sublimetext for editing config files
alias zshconfig="code ~/.zshrc"
alias envconfig="code ~/Projects/config/env.sh"
alias cat='bat'
alias preview="fzf --preview 'bat --color \"always\" {}'"
alias be="bundle exec"
